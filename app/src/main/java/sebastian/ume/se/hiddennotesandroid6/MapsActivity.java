package sebastian.ume.se.hiddennotesandroid6;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Location;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import android.location.LocationManager;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;

import android.support.v7.app.AppCompatActivity;
import android.app.AlertDialog;


import static android.content.pm.PackageManager.PERMISSION_DENIED;
import static android.content.pm.PackageManager.PERMISSION_GRANTED;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private FusedLocationProviderClient mFusedLocationClient;
    private GoogleMap mMap;
    private GoogleApiClient googleApiClient;
    private static final int LOCATION_REQUEST_CODE = 101;
    private DrawerLayout mDrawerLayout;
    private FloatingActionMenu materialDesignFAM;
    private FloatingActionButton floatingActionButton1, floatingActionButton2;
    private Intent myIntent;
    private Location location;
    private DatabaseHelper mDatabaseHelper;
    private float smallestdistans = 100;
    private int _ID;
    private String _Title,_Message,_date;
    private boolean asked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        mDatabaseHelper = new DatabaseHelper(this);
        // Toolbar and layout
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);
        mDrawerLayout = findViewById(R.id.drawer_layout);
        // Drawer menu
        this.drawerMenu();
        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.FAB);
        floatingActionButton1 = (FloatingActionButton) findViewById(R.id.Reed);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.Write);
        this.floatRead();
        this.floatWrite();
        asked = false;
        mapSetup();


    }

    public void floatWrite(){
        // floating button write note
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu second item clicked
                checkLocation();
                Cursor data = mDatabaseHelper.getData();
                smallestdistans = 100;
                while(data.moveToNext()){
                    Location savedlocation = new Location("dummyprovider");;
                    savedlocation.setLatitude(data.getDouble(5));
                    savedlocation.setLongitude(data.getDouble(4));
                    setSmallestdistans(savedlocation);
                }
                // Check so there is no note already in a note area
                if (smallestdistans > 15) {
                    // distance between first and second location is larger than 15m
                    materialDesignFAM.close(true);
                    myIntent = new Intent(MapsActivity.this, WriteActivity.class);
                    myIntent.putExtra("locationLong", location.getLongitude());
                    myIntent.putExtra("locationLat", location.getLatitude());
                    MapsActivity.this.startActivity(myIntent);
                }else{
                    //Toast.makeText(MapsActivity.this,getString(R.string.alreadynote) , Toast.LENGTH_SHORT).show();
                    // In case of there already is a note within 15 meters we open the existing note.
                    myIntent = new Intent(MapsActivity.this, ReadActivity.class);
                    myIntent.putExtra("messageID", _ID);
                    myIntent.putExtra("messageTitle", _Title);
                    myIntent.putExtra("messageMessage", _Message);
                    myIntent.putExtra("messageDate", _date);
                    MapsActivity.this.startActivity(myIntent);
                }
            }
        });
    }

    public void floatRead(){
        // Floating button read note
        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //TODO something when floating action menu first item clicked
                materialDesignFAM.close(true);
                checkLocation();
                Cursor data = mDatabaseHelper.getData();
                if (data.getCount() == 0){
                    Toast.makeText(MapsActivity.this, getString(R.string.nonotes), Toast.LENGTH_SHORT).show();

                }
                while(data.moveToNext()){
                    Location savedlocation = new Location("dummyprovider");;
                    savedlocation.setLatitude(data.getDouble(5));
                    savedlocation.setLongitude(data.getDouble(4));
                    // firstLocation and secondLocation are Location class instances
                    float distance = location.distanceTo(savedlocation); // distance in meters
                    // Check the closest note
                    if(distance < smallestdistans){
                        smallestdistans = distance;
                        _ID = data.getInt(0);
                        _Title = data.getString(1);
                        _Message = data.getString(2);
                        _date = data.getString(3);
                    }
                }
                // opens all notes that is closer then 15 meters
                if (smallestdistans < 15) {
                    // distance between first and second location is less than 15m
                    myIntent = new Intent(MapsActivity.this, ReadActivity.class);
                    myIntent.putExtra("messageID", _ID);
                    myIntent.putExtra("messageTitle", _Title);
                    myIntent.putExtra("messageMessage", _Message);
                    myIntent.putExtra("messageDate", _date);
                    MapsActivity.this.startActivity(myIntent);

                    // skicka över id, titel, meddelande
                }else{
                    Toast.makeText(MapsActivity.this, getString(R.string.nonotesnearby), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void drawerMenu(){
        final NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setCheckedItem(R.id.nav_map);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        // set item as selected to persist highlight
                        menuItem.setChecked(false);
                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();
                        // code to update the UI
                        int id = menuItem.getItemId();
                        switch (id){
                            case R.id.nav_map:
                                mDrawerLayout.closeDrawers();
                                break;
                            case R.id.nav_messages:
                                checkLocation();
                                myIntent = new Intent(MapsActivity.this, MessageActivity.class);
                                myIntent.putExtra("myLong", location.getLongitude());
                                myIntent.putExtra("myLat", location.getLatitude());
                                MapsActivity.this.startActivity(myIntent);
                                break;
                            case R.id.nav_help:
                                myIntent = new Intent(MapsActivity.this, HelpActivity.class);
                                MapsActivity.this.startActivity(myIntent);
                                break;
                            case R.id.nav_contact:
                                myIntent = new Intent(MapsActivity.this, ContactActivity.class);
                                MapsActivity.this.startActivity(myIntent);
                                break;
                        }
                        return true;
                    }
                });
    }

    public void setSmallestdistans(Location savedLocation){
        // firstLocation and secondLocation are Location class instances
        float distance = location.distanceTo(savedLocation); // distance in meters
        // Check the closest note
        if(distance < smallestdistans){
            smallestdistans = distance;
        }
    }

    public void mapSetup(){
        // Map
        //Instantiating the GoogleApiClient
        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        checkLocation();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.toolbar_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
            case R.id.Update_button:
                //mapSetup();
                recreate();
                Toast.makeText(MapsActivity.this,getString(R.string.Locationupdate) , Toast.LENGTH_SHORT).show();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        checkLocation();
    }

    public void onStart() {
        super.onStart();
        // Initiating the connection
        googleApiClient.connect();

    }

    public void onStop() {
        super.onStop();
        // Disconnecting the connection
        googleApiClient.disconnect();

    }

    //Callback invoked once the GoogleApiClient is connected successfully
    public void onConnected(Bundle bundle) {
        SupportMapFragment mapFragment = (SupportMapFragment)
                getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    //Callback invoked if the GoogleApiClient connection fails
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PERMISSION_GRANTED) {
                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            checkLocation();
                        }
                    }, 100);
                } else if (grantResults.length > 0 && grantResults[0] == PERMISSION_DENIED){
                    asked = true;
                    Toast.makeText(this, getString(R.string.PermissionDenied), Toast.LENGTH_SHORT).show();
                    myIntent = new Intent(MapsActivity.this, HelpActivity.class);
                    MapsActivity.this.startActivity(myIntent);
                }
                break;
        }
    }

    private void checkLocation() {
        //Checking if the user has granted the permission
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED ) {
            //Requesting the Location permission
            if (asked){
                return;
            }
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION}, LOCATION_REQUEST_CODE);
            return;
        }else{
            // Checking so GPS is activated
            //statusCheck();
            //Fetching the last known location using the Fus
            //location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location loc) {
                            // Got last known location. In some rare situations, this can be null.
                            if (loc != null) {
                                // Logic to handle location object
                            }
                            location = loc;
                        }
                    });
            if (location == null){
                return;
            }

            // XX gör detta någon skillnad annars tabort
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    AddToMap();
                }
            }, 100);
        }


    }

    private void AddToMap(){
        //MarkerOptions are used to create a new Marker.You can specify location, title etc with MarkerOptions
        MarkerOptions markerOptions = new MarkerOptions()
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .title(getString(R.string.yourlocation))
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));

        //Adding the created the marker on the map
        mMap.addMarker(markerOptions);

        // Move the camera instantly to location with a zoom of 15.
        LatLng cameraZoom = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(cameraZoom, 15));

        AddDatabaseToMap();

    }

    // Adds a circle for each note in the database
    public void AddDatabaseToMap(){
        Cursor data = mDatabaseHelper.getData();
        while(data.moveToNext()){

            CircleOptions circleOptions=new CircleOptions();
            circleOptions.center(new LatLng(data.getDouble(5), data.getDouble(4)));
            circleOptions.radius(15);
            circleOptions.strokeColor(ContextCompat.getColor(this,android.R.color.holo_orange_dark));
            circleOptions.fillColor(0x220000FF);

            mMap.addCircle(circleOptions);

            //MarkerOptions are used to create a new Marker.You can specify location, title etc with MarkerOptions
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLng(data.getDouble(5), data.getDouble(4)))
                    .title(data.getString(1))
                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

            //Adding the created the marker on the map
            mMap.addMarker(markerOptions);
        }
    }

    // Checks if GPS is enabled
    public void statusCheck() {
        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }
    }

    // If GPS is disabled build a popup to pront user to aktivate it
    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(getString(R.string.GPSoff))
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }
    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        if(ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED){
            if (mMap != null){
                mMap.clear();
                mapSetup();
            }
        }
        asked = false;
    }
}