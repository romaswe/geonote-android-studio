package sebastian.ume.se.hiddennotesandroid6;

import android.app.ActionBar;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.MessageDigest;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class WriteActivity extends AppCompatActivity {
    private EditText titleText;
    private EditText messageText;
    private double longitude;
    private double latitude;
    private DateFormat dateFormat;
    private Date date;
    private String encryptedMessage;
    private String AES = "AES";

    private DatabaseHelper mDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_write);

        // Database
        mDatabaseHelper = new DatabaseHelper(this);

        // Intent extra data
        Bundle myBundle = getIntent().getExtras();
        longitude = myBundle.getDouble("locationLong");
        latitude = myBundle.getDouble("locationLat");


        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        // Sets format for date
        dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        date = new Date();

        // Layout
        titleText = findViewById(R.id.InputTitle);
        messageText = findViewById(R.id.InputMessage);
        this.submitButton();
    }

    public void submitButton(){
        final Button submit = findViewById(R.id.SaveMessage);

        // Submitbutton to add note to database
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String title = titleText.getText().toString();
                String message = messageText.getText().toString();
                if(title != null && !title.isEmpty()){

                    // Encrypt message before adding it to database
                    try {
                        encryptedMessage = encrypt(message, getString(R.string.password));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    System.out.println(encryptedMessage);
                    System.out.println(message);

                    AddData(title,encryptedMessage,dateFormat.format(date),longitude,latitude);

                    finish();

                }else {
                    Toast.makeText(WriteActivity.this, getString(R.string.MessageMissingTitle), Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    /**
     * Encrypton functon to take a message and keybased enkrypt it
     * @param message string of message that should be encrypted
     * @param password string of password to encrypt the message
     * @return encrypted string
     */
    private String encrypt(String message, String password) throws Exception{
        SecretKeySpec key = generateKey(password);
        Cipher c = Cipher.getInstance(AES);
        c.init(Cipher.ENCRYPT_MODE,key);
        byte[] encVal = c.doFinal(message.getBytes());
        String encryptedValue = Base64.encodeToString(encVal,Base64.DEFAULT);
        return  encryptedValue;
    }
    /**
     *  Key generating function that genreates a key based on a string
     * @param password string of password that the key is based on
     * @return key for the encrypton
     */
    private SecretKeySpec generateKey(String password) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = password.getBytes("UTF-8");
        digest.update(bytes,0,bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /**
     * Function call to add a note to the database
     * @param title String of the messages title
     * @param message String of the message
     * @param date String of the date the note was made
     * @param longitude Double value of the longitude
     * @param latitude Double Value of the latitude
     */
    public void AddData(String title, String message, String date, Double longitude, Double latitude) {
        boolean insertData = mDatabaseHelper.addData(title, message, date, longitude, latitude);

        if (insertData) {
            Toast.makeText(WriteActivity.this, getString(R.string.successinsert), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(WriteActivity.this, getString(R.string.noinsert), Toast.LENGTH_SHORT).show();

        }
    }
}
