package sebastian.ume.se.hiddennotesandroid6;

import android.content.Intent;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MessageActivity extends AppCompatActivity {

    private static final String TAG = "MessageActivity";

    private DatabaseHelper mDatabaseHelper;

    private ListView mListView;

    private double myLongitude, myLatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        // Layout
        mListView = (ListView) findViewById(R.id.listView);
        mDatabaseHelper = new DatabaseHelper(this);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        // Functoncall to show all messages
        populateListView();

        Intent receivedIntent = getIntent();
        myLongitude = receivedIntent.getDoubleExtra("myLong", -1);
        myLatitude = receivedIntent.getDoubleExtra("myLat", -1);

    }
    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    /**
     * Gets all instances in the database and display title and date for each note in the database
     */
    private void populateListView() {
        Log.d(TAG, "populateListView: Displaying data in the ListView.");

        //get the data and append to a list
        Cursor data = mDatabaseHelper.getData();
        ArrayList<String> listData = new ArrayList<>();
        final ArrayList<String> titles = new ArrayList<>();
        final ArrayList<String> messages = new ArrayList<>();
        final ArrayList<String> dates = new ArrayList<>();
        final ArrayList<Double> longetude = new ArrayList<>();
        final ArrayList<Double> latitude = new ArrayList<>();
        while(data.moveToNext()){
            //get the value from the database in column 1 and 3
            //then add it to the ArrayList
            listData.add("Title: " + data.getString(1) + "\nDate: " + data.getString(3));

            titles.add(data.getString(1));
            messages.add(data.getString(2));
            dates.add(data.getString(3));
            longetude.add(data.getDouble(4));
            latitude.add(data.getDouble(5));


        }
        //create the list adapter and set the adapter
        ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
        mListView.setAdapter(adapter);

        //set an onItemClickListener to the ListView
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemClick: You Clicked " + i);
                Log.d(TAG, "onItemClick: You Clicked on " + titles.get(i));

                Cursor data = mDatabaseHelper.getItemID(titles.get(i),messages.get(i),dates.get(i), longetude.get(i),latitude.get(i)); //get the id associated with that name
                int itemID = -1;
                while(data.moveToNext()){
                    itemID = data.getInt(0);
                }
                if(itemID > -1){
                    Log.d(TAG, "onItemClick: The ID is: " + itemID);
                    Intent editScreenIntent = new Intent(MessageActivity.this, EditActivity.class);
                    editScreenIntent.putExtra("id",itemID);
                    editScreenIntent.putExtra("title",titles.get(i));
                    editScreenIntent.putExtra("myLong", myLongitude);
                    editScreenIntent.putExtra("myLat", myLatitude);
                    editScreenIntent.putExtra("noteLong", longetude.get(i));
                    editScreenIntent.putExtra("noteLat", latitude.get(i));

                    startActivity(editScreenIntent);
                }
                else{
                    Toast.makeText(MessageActivity.this ,getString(R.string.noId), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    public void onResume()
    {  // After a pause OR at startup
        super.onResume();
        //Refresh your stuff here
        populateListView();
    }
}
