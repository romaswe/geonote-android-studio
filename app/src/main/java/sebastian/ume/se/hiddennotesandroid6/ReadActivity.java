package sebastian.ume.se.hiddennotesandroid6;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class ReadActivity extends AppCompatActivity {

    private String messageTitle,messageMessage, messageDate;
    private Integer messageID;
    private Button btnSave,btnDelete;
    private DatabaseHelper mDatabaseHelper;
    private EditText titleText;
    private EditText messageText;
    private String encryptedMessage, decryptedMessage;
    private String AES = "AES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reed);

        // toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        // Intend extra data
        Bundle myBundle = getIntent().getExtras();
        messageID = myBundle.getInt("messageID");
        messageTitle = myBundle.getString("messageTitle");
        messageMessage = myBundle.getString("messageMessage");
        messageDate = myBundle.getString("messageDate");

        // layout
        titleText = (EditText)findViewById(R.id.InputTitle);
        titleText.setText(messageTitle, TextView.BufferType.EDITABLE);

        messageText = (EditText)findViewById(R.id.InputMessage);
        // Decrypt message before showing it
        try {
            decryptedMessage = decrypt(messageMessage, getString(R.string.password));
        } catch (Exception e) {
            e.printStackTrace();
        }
        messageText.setText(decryptedMessage, TextView.BufferType.EDITABLE);

        btnSave = (Button) findViewById(R.id.SaveMessage);
        btnDelete = (Button) findViewById(R.id.DeleteMessage);
        mDatabaseHelper = new DatabaseHelper(this);

        // Button to save data
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = titleText.getText().toString();
                String message = messageText.getText().toString();
                if(!title.equals("")){
                    // Encrypt message before adding it to database
                    try {
                        encryptedMessage = encrypt(message, getString(R.string.password));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    mDatabaseHelper.updateTitle(title,messageID,messageTitle);
                    mDatabaseHelper.updateMessage(encryptedMessage,messageID,messageMessage);
                    finish();
                }else{
                    Toast.makeText(ReadActivity.this, getString(R.string.MessageMissingTitle), Toast.LENGTH_SHORT).show();
                }

            }
        });

        // Button to delete data
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatabaseHelper.deleteNote(messageID,messageTitle);
                Toast.makeText(ReadActivity.this, getString(R.string.deletedata), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    /**
     * Decryption function to decrypt the message stored in the database
     * @param messageMessage string of the encrypted message
     * @param password string of the password to decrypt
     * @return decrypted message
     */
    private String decrypt(String messageMessage, String password) throws Exception {
        SecretKeySpec key = generateKey(password);
        Cipher c = Cipher.getInstance(AES);
        c.init(Cipher.DECRYPT_MODE,key);
        byte[] decodedValue = Base64.decode(messageMessage,Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodedValue);
        String decryptedValue = new String(decValue);
        return  decryptedValue;
    }

    /**
     * Encrypton functon to take a message and keybased enkrypt it
     * @param message string of message that should be encrypted
     * @param password string of password to encrypt the message
     * @return encrypted string
     */
    private String encrypt(String message, String password) throws Exception{
        SecretKeySpec key = generateKey(password);
        Cipher c = Cipher.getInstance(AES);
        c.init(Cipher.ENCRYPT_MODE,key);
        byte[] encVal = c.doFinal(message.getBytes());
        String encryptedValue = Base64.encodeToString(encVal,Base64.DEFAULT);
        return  encryptedValue;
    }

    /**
     *  Key generating function that genreates a key based on a string
     * @param password string of password that the key is based on
     * @return key for the encrypton
     */
    private SecretKeySpec generateKey(String password) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = password.getBytes("UTF-8");
        digest.update(bytes,0,bytes.length);
        byte[] key = digest.digest();
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
