package sebastian.ume.se.hiddennotesandroid6;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Date;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String TABLE_NAME = "message_table";
    private static final String COL1 = "ID";
    private static final String COL2 = "title";
    private static final String COL3 = "message";
    private static final String COL4 = "dateOfnote";
    private static final String COL5 = "longitude";
    private static final String COL6 = "latitude";

    public DatabaseHelper(Context context) {
        super(context, TABLE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String createTable = "CREATE TABLE " + TABLE_NAME + " (ID INTEGER PRIMARY KEY AUTOINCREMENT, " + COL2 +" TEXT," + COL3 + " TEXT," + COL4 + " TEXT," + COL5 + " REAL," + COL6 + " REAL)";
        sqLiteDatabase.execSQL(createTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    /**
     * Adds data to the database
     * @param title String of the messages title
     * @param message String of the message
     * @param date String of the date the note was made
     * @param longitude Double value of the longitude
     * @param latitude Double Value of the latitude
     * @return boolean if the data insert was successful or not
     */
    public boolean addData(String title, String message, String date, Double longitude, Double latitude) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COL2, title);
        contentValues.put(COL3, message);
        contentValues.put(COL4, date);
        contentValues.put(COL5, longitude);
        contentValues.put(COL6, latitude);

        Log.d(TAG, "addData: Adding " + title +", "+ message + ", " + date + ", " + longitude + ", " + latitude + " to " + TABLE_NAME);

        long result = db.insert(TABLE_NAME, null, contentValues);

        //if date as inserted incorrectly it will return -1
        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Function to get all the data in the table
     * @return Cursor datatype of the table
     */
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    /**
     * Function to get the ID of a item
     * @param title String of the messages title
     * @param message String of the message
     * @param date String of the date the note was made
     * @param longitude Double value of the longitude
     * @param latitude Double Value of the latitude
     * @return id of the selected object
     */
    public Cursor getItemID(String title, String message, String date, Double longitude, Double latitude){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COL1 + " FROM " + TABLE_NAME +
                " WHERE " + COL2 + " = '" + title + "'" + " AND " + COL3 + " = '" + message + "'" + " AND " + COL4 + " = '" + date + "'" + " AND " + COL5 + " = '" + longitude + "'" + " AND " + COL6 + " = '" + latitude + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }

    /**
     * Function to change/edit the title
     * @param newTitle String with new title
     * @param id integer with the id
     * @param oldTitle String with old title
     */
    public void updateTitle(String newTitle, int id, String oldTitle){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + COL2 +
                " = '" + newTitle + "' WHERE " + COL1 + " = '" + id + "'" +
                " AND " + COL2 + " = '" + oldTitle + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newTitle);
        db.execSQL(query);
    }

    /**
     * Function to change/edit message
     * @param newMessage String with new message
     * @param id integer with the id
     * @param oldMessage String with old title
     */
    public void updateMessage(String newMessage, int id, String oldMessage){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_NAME + " SET " + COL3 +
                " = '" + newMessage + "' WHERE " + COL1 + " = '" + id + "'" +
                " AND " + COL3 + " = '" + oldMessage + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newMessage);
        db.execSQL(query);
    }

    /**
     * Function to delete note from database
     * @param id integer with the id
     * @param title String with the title
     */
    public void deleteNote(int id, String title){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_NAME + " WHERE "
                + COL1 + " = '" + id + "'" +
                " AND " + COL2 + " = '" + title + "'";
        Log.d(TAG, "deleteName: query: " + query);
        Log.d(TAG, "deleteName: Deleting " + title + " from database.");
        db.execSQL(query);
    }

    /**
     * Function to get a single note
     * @param id integer with the id
     * @return single object from the database
     */
    /*public Cursor getSingleItem(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_NAME + " WHERE " + COL1 + " = '" + id + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }*/
    }
