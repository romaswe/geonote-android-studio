package sebastian.ume.se.hiddennotesandroid6;

import android.content.Intent;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

public class EditActivity extends AppCompatActivity {

    private static final String TAG = "EditActivity";

    private Button btnDelete;

    private DatabaseHelper mDatabaseHelper;

    private String selectedTitle;
    private int selectedID;

    double myLongitude, myLatitude, noteLongitude, noteLatitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back_white_24dp);

        btnDelete = (Button) findViewById(R.id.btnDelete);
        mDatabaseHelper = new DatabaseHelper(this);

        //get the intent extra from the MessageActivity
        Intent receivedIntent = getIntent();
        selectedID = receivedIntent.getIntExtra("id",-1); //NOTE: -1 is just the default value
        selectedTitle = receivedIntent.getStringExtra("title");
        myLongitude = receivedIntent.getDoubleExtra("myLong", -1);
        myLatitude = receivedIntent.getDoubleExtra("myLat", -1);
        noteLongitude = receivedIntent.getDoubleExtra("noteLong", -1);
        noteLatitude = receivedIntent.getDoubleExtra("noteLat", -1);

        Location myLocation = new Location("dummyprovider");;
        myLocation.setLatitude(myLatitude);
        myLocation.setLongitude(myLongitude);

        Location noteLocation = new Location("dummyprovider");;
        noteLocation.setLatitude(noteLatitude);
        noteLocation.setLongitude(noteLongitude);

        // firstLocation and secondLocation are Location class instances
        float distance = myLocation.distanceTo(noteLocation); // distance in meters

        DecimalFormat precision = new DecimalFormat("0.00");

        final TextView textViewToChange = (TextView) findViewById(R.id.item);
        textViewToChange.setText(getString(R.string.titleNote) + " " + selectedTitle);

        final TextView Textdistance = (TextView) findViewById(R.id.distance);
        Textdistance.setText(getString(R.string.distanceNote)+ " " + precision.format(distance));

        // Button to delete message
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDatabaseHelper.deleteNote(selectedID,selectedTitle);
                Toast.makeText(EditActivity.this, getString(R.string.DeleteMessage), Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
